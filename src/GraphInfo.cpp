#include <cstring>
#include <iostream>
#include "lib/Graph.h"
#include "lib/Translator.h"
#include "solutions/HalfIntBranching.h"
#include "solutions/SolutionVariants.h"

using namespace std;

// Takes solution kind as argument
int main(int argc, char *argv[]) {
    Graph g;
    Translator t;

    t.read_input(&g);
    BranchingSolution *s = new BranchingSolution();
    
    while(true) {
            ReductionResult r = s->simpleReductions(g);
            if (r != RED_CHANGED)
                break;
        }


    
    vector<pair<int, int> > edges = g.createAllEdgeList();
    for(auto x: edges) 
        cout << x.first << " " <<  x.second << "\n";

    g.printStats();
    delete s;
    // cout << sol.size() << "\n";
    // t.printNames(sol);

    return 0;
}
