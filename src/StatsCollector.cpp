#include <cstring>
#include <iostream>
#include "lib/Graph.h"
#include "lib/Translator.h"

using namespace std;

// Takes solution kind as argument
int main(int argc, char *argv[]) {
    Graph g;
    Translator t;
    
    t.read_input(&g);

    cout << g.getN() << "," << g.getM() << "," << g.getMaxDegV().second << "\n";
    
    return 0;
}
